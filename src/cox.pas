(* * * * * * * * * * * * * * * ==== COX ==== * * * * * * * * * * * * * *
 *      Version: 1.0.0 beta-1                                          *
 * Release date: June 12th, 2014                                       *
 *      License: GNU GPL 3.0  (included with the source)               *
 *       Author: Igor Nunes, aka thoga31 @ www.portugal-a-programar.pt *
 *                                                                     *
 *  Description: Compiler Output eXtender                              *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)

{$mode objfpc}
program cox;
uses crt, classes, strutils, sysutils, process,
     UTypes, UUIIntegrator, UAppInfo;

var compiler    : TProcess;                   // Calls the compiler.
    listOfComp  : array of TCompiler = nil;   // List of all compilers available on "compilers.def" - must be processed by COXgen.
    colorscheme : TCompiler;                  // Stores the information of the current compiler in use.
    modifier    : TAppModifier = ModDefault;  // Defines the output.



procedure GetCompilers;
(* Loads all compilers available from "compilers.def" *)
var f : file of TCompiler;
begin
    if not FileExists(ExtractFilePath(ParamStr(0)) + FILE_BIN_NAME) then  // It is absolutely necessary!
        raise Exception.Create('Essential file ''' + FILE_BIN_NAME + ''' not found.')
    else begin
        Assign(f, ExtractFilePath(ParamStr(0)) + FILE_BIN_NAME);
        Reset(f);
        while not eof(f) do begin
            SetLength(listOfComp, Length(listOfComp)+1);
            read(f, listOfComp[High(listOfComp)]);
        end;
        Close(f);
    end;
end;


procedure ValidateParameters;
(* Validates parameters, changes the output modifier and gets current compiler. *)

    function IsAccessValid(a : string; out comp : TCompiler) : boolean;
    (* Is the called compiler available in "compilers.def"? *)
    var elem : TCompiler;
    begin
        IsAccessValid := false;
        for elem in listOfComp do
            if UpCase(elem.access) = UpCase(a) then begin
                IsAccessValid := true;
                comp := elem;
                break;
            end;
    end;
    
    function IsModifier(p : string) : boolean;
    (* Checks if "p" is a modifier and, if so, changes the output modifier. *)
    begin
        IsModifier := true;
        case p of
            '-d' : begin
                       writeln('COX Info: Running in debugger mode.');
                       modifier := ModDebug;
                   end;
            '-q' : begin
                       writeln('COX Info: Running in quiet mode.');
                       modifier := ModQuiet;
                   end;
        else
            IsModifier := false;
        end;
    end;

var i    : byte = 1;
    comp : TCompiler;

begin {ValidateParameters}
    if IsModifier(ParamStr(i)) then begin
        Inc(i);  // Compiler is the next parameter
    end;
    
    if not IsAccessValid(ParamStr(i), comp) then begin
        raise Exception.Create('Access parameter ''' + ParamStr(i) + ''' is not recognized.');
    end else begin
        compiler.Executable := {$ifdef windows} comp.binary.windows {$endif}
                               {$ifdef unix}    comp.binary.unix    {$endif};
        Inc(i);
        while i <= ParamCount do begin
            compiler.Parameters.Add(ParamStr(i));
            Inc(i);
        end;
        writeln('COX Info: Using ''', comp.access, ''' (', comp.language, ').');
        colorscheme := comp;
    end;
end; {ValidateParameters}


procedure Compile;
(* Calls compiler and colorizes the output. *)
const READ_BYTES = 2048;

    procedure WriteColorized(const s : string);
    (* What the identifier says xD *)
    var newcolor : TColorDefinition;
    begin
        textbackground(colorscheme.defaultcolor.bkcolor);
        textcolor(colorscheme.defaultcolor.txcolor);
        for newcolor in colorscheme.customcolor do
            if AnsiContainsText(s, newcolor.msg) then begin
                textbackground(newcolor.bkcolor);
                textcolor(newcolor.txcolor);
            end;
        writeln(s);
        textbackground(0);
        textcolor(7);
    end;

var OutputLines: TStringList;
    MemStream: TMemoryStream;
    NumBytes: LongInt;
    BytesRead: LongInt = 0;

begin {Compile}
    MemStream := TMemoryStream.Create;
    try
    try
        if modifier = ModDebug then begin
            writeln('COX Debug: COX started.');
            writeln('COX Debug: run started');
        end;
        
        compiler.Execute;
        while true do begin          
            MemStream.SetSize(BytesRead + READ_BYTES);
            NumBytes := compiler.Output.Read((MemStream.Memory + BytesRead)^, READ_BYTES);
            if NumBytes > 0 then begin
                Inc(BytesRead, NumBytes);
                if modifier = ModDebug then
                    write('.')
            end else 
                break;
        end;
        
        if BytesRead > 0 then
            WriteLn;
        
        MemStream.SetSize(BytesRead);
        if modifier = ModDebug then
            writeLn('COX Debug: Compiler run complete');

        OutputLines := TStringList.Create;
        OutputLines.LoadFromStream(MemStream);
        if modifier = ModDebug then
            writeLn('COX Debug: Compiler output line count = ', OutputLines.Count);
        
        if modifier <> ModQuiet then begin
            textcolor(15); writeln(' ', DupeString('-', SCREENWIDTH-2));
            for NumBytes := 0 to OutputLines.Count - 1 do
                WriteColorized(OutputLines[NumBytes]);
            textcolor(15); writeln(' ', DupeString('-', SCREENWIDTH-2)); textcolor(7);
        end;
    except
        on ex : exception do
            ShowException(ex);
    end
    finally
        if modifier = ModDebug then
            WriteLn('COX Debug: COX end');
        
        OutputLines.Free;
        MemStream.Free;
    end
end; {Compile}



begin
    ShowHeader;
    compiler := TProcess.Create(nil);
    
    try
    try
        if not SpecialModes then begin  // SpecialModes = help and about - if so, runs the special mode (in "UAppInfo")
            GetCompilers;
            ValidateParameters;
            compiler.Options := compiler.Options + [poUsePipes];
            Compile;
        end;
    except
        on ex : exception do
            ShowException(ex);
    end
    finally
        compiler.Free;
    end;
    writeln('Done.');
    writeln;
end.
