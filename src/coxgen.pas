(* * * * * * * * * * * * * * * === COXgen ===  * * * * * * * * * * * * *
 *      Version: 1.0.0 beta-1                                          *
 * Release date: June 12th, 2014                                       *
 *      License: GNU GPL 3.0  (included with the source)               *
 *       Author: Igor Nunes, aka thoga31 @ www.portugal-a-programar.pt *
 *                                                                     *
 *  Description: COX Binary File Generator                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)

{$mode objfpc}
program coxgen;
uses sysutils, strutils, UTypes, UUIIntegrator, UMisc;

const PARSER_DEFINE           = 'DEFINE ';
      PARSER_ENDDEFINE        = 'END DEFINE';
      PARSER_ACCESS           = 'ACCESS = ';
      PARSER_COMPILER         = 'COMPILER = {';
      PARSER_COMPILER_WINDOWS = 'WINDOWS = ';
      PARSER_COMPILER_UNIX    = 'UNIX = ';
      PARSER_MESSAGES         = 'MESSAGES = {';
      PARSER_MESSAGES_DEFAULT = 'DEFAULT = ';
      PARSER_MESSAGES_NEW     = 'NEW ';
      PARSER_ARGSEPARATOR     = ',';
      PARSER_EQUALS           = '=';
      PARSER_STARTSECTION     = '{';
      PARSER_ENDSECTION       = '}';
      PARSER_ARGDELIMITER     = '"';
      PARSER_SPACER           = ' ';

var f_text     : text;
    f_bin      : file of TCompiler;
    listOfComp : array of TCompiler = nil;
    line       : string = '';
    current    : word = 0;
    curcolor   : word = 0;
    temparr    : TStringArray;
    i, j       : word;
    modifier   : TAppModifier = ModDefault;


procedure Normalize(var s : string);
(* Removes unnecessary spaces and other "special chars" *)
begin
    s := Tab2Space(s, 0);
    s := DelSpace1(s);
    s := Trim(s);
end;


begin  // Yeah, this isn't beatiful xD
    try
    try
        writeln('For more information about COXgen, see ''cox --help''.');
        
        if ParamCount > 0 then
            case ParamStr(1) of
                '-d' : begin
                           writeln('COXgen Info: Running in debugger mode.');
                           modifier := ModDebug;
                       end;
                '-q' : begin
                           writeln('COXgen Info: Running in quiet mode.');
                           modifier := ModQuiet;
                       end;
            end;
        
        if modifier <> ModQuiet then
            writeln('COXgen Info: Processing file...');
        
        if FileExists(ExtractFilePath(ParamStr(0)) + FILE_TEXT_NAME) then begin
            Assign(f_text, ExtractFilePath(ParamStr(0)) + FILE_TEXT_NAME);
            Reset(f_text);
        end else
            raise Exception.Create('Essential file ''' + FILE_TEXT_NAME + ''' not found.');
        
        if modifier = ModDebug then
            writeln('COXgen Debug: Parsing file...');
        
        while not eof(f_text) do begin
            readln(f_text, line);
            Normalize(line);
            
            if AnsiStartsText(PARSER_DEFINE, line) then begin
                if modifier = ModDebug then
                    write('   New definition: ');
                
                Inc(current);
                SetLength(listOfComp, current);
                Delete(line, 1, Length(PARSER_DEFINE));
                listOfComp[current-1].language := line;
                if modifier = ModDebug then
                    writeln(line);
            end
            
            else if AnsiStartsText(PARSER_ACCESS, line) then begin
                if modifier = ModDebug then
                    write('      Access: ');
                
                Delete(line, 1, Length(PARSER_ACCESS));
                listOfComp[current-1].access := line;
                if modifier = ModDebug then
                    writeln(line);
            end
            
            else if AnsiStartsText(PARSER_COMPILER, line) then begin
                if modifier = ModDebug then
                    writeln('      Getting compilers...');
                
                readln(f_text, line);
                Normalize(line);
                
                while line <> PARSER_ENDSECTION do begin
                    if AnsiStartsText(PARSER_COMPILER_WINDOWS, line) then begin
                        if modifier = ModDebug then
                            write('         Windows: ');
                        
                        Delete(line, 1, Length(PARSER_COMPILER_WINDOWS));
                        listOfComp[current-1].binary.windows := line;
                        if modifier = ModDebug then
                            writeln(line);
                    end
                    else if AnsiStartsText(PARSER_COMPILER_UNIX, line) then begin
                        if modifier = ModDebug then
                            write('         Unix: ');
                        
                        Delete(line, 1, Length(PARSER_COMPILER_UNIX));
                        listOfComp[current-1].binary.unix := line;
                        if modifier = ModDebug then
                            writeln(line);
                    end else
                        raise Exception.Create('Invalid compiler definition, "' + line + '".');
                    readln(f_text, line);
                    Normalize(line);
                end;
            end
            
            else if AnsiStartsText(PARSER_MESSAGES, line) then begin
                if modifier = ModDebug then
                    writeln('      Getting messages...');
                
                readln(f_text, line);
                Normalize(line);
                
                curcolor := 1;
                while line <> PARSER_ENDSECTION do begin
                    if AnsiStartsText(PARSER_MESSAGES_DEFAULT, line) then begin
                        if modifier = ModDebug then
                            writeln('         Default:');
                        
                        Delete(line, 1, Length(PARSER_MESSAGES_DEFAULT));
                        temparr := Split(line, ',');
                        if modifier = ModDebug then
                            writeln('            * Line "', line, '" splited.');
                        
                        if Length(temparr) > 2 then
                            raise Exception.Create('More than two colors defined ("' + line + '").');
                        listOfComp[current-1].defaultcolor.msg     := '';
                        if modifier = ModDebug then
                            writeln('            * bk=', temparr[0], ', tx=', temparr[1]);
                        
                        listOfComp[current-1].defaultcolor.bkcolor := StrToInt(temparr[0]);
                        listOfComp[current-1].defaultcolor.txcolor := StrToInt(temparr[1]);
                    end
                    else if AnsiStartsText(PARSER_MESSAGES_NEW, line) then begin
                        if modifier = ModDebug then
                            writeln('         New:');
                        
                        Delete(line, 1, Length(PARSER_MESSAGES_NEW));
                        listOfComp[current-1].customcolor[curcolor].msg := ExtractFromQuote(line);
                        if modifier = ModDebug then
                            writeln('            * msg="', listOfComp[current-1].customcolor[curcolor].msg, '"');
                        
                        line := AnsiReplaceText(line, PARSER_EQUALS, '');
                        line := Trim(line);
                        temparr := Split(line, ',');
                        if modifier = ModDebug then
                            writeln('            * Line "', line, '" splited.');
                        
                        if Length(temparr) > 2 then
                            raise Exception.Create('More than two colors defined ("' + line + '").');
                        if modifier = ModDebug then
                            writeln('            * bk=', temparr[0], ', tx=', temparr[1]);
                        
                        listOfComp[current-1].customcolor[curcolor].bkcolor := StrToInt(temparr[0]);
                        listOfComp[current-1].customcolor[curcolor].txcolor := StrToInt(temparr[1]);
                        Inc(curcolor);
                    end else
                        raise Exception.Create('Invalid message definition, "' + line + '".');
                    readln(f_text, line);
                    Normalize(line);
                end; 
            
            end else if line = PARSER_ENDDEFINE then begin
                if modifier = ModDebug then begin
                    writeln('End of new definition.');
                    writeln;
                end;
                // void
            end else if line <> '' then
                raise Exception.Create('Syntax not recognized: "' + line + '".');
        end;
        
        if modifier <> ModQuiet then begin
            writeln('COXgen Info: Writing information obtained...');
            for i := Low(listOfComp) to High(listOfComp) do begin
                writeln('   Definition "', listOfComp[i].language, '":');
                writeln('      Access = ', listOfComp[i].access);
                writeln('      Binary:');
                writeln('         Windows = ', listOfComp[i].binary.windows);
                writeln('         Unix    = ', listOfComp[i].binary.unix);
                writeln('      Messages:');
                writeln('         Default = ', listOfComp[i].defaultcolor.bkcolor:2, ', ', listOfComp[i].defaultcolor.txcolor:2);
                j := 1;
                while listOfComp[i].customcolor[j].msg <> '' do begin
                    writeln('         New "', listOfComp[i].customcolor[j].msg, '" = ', listOfComp[i].customcolor[j].bkcolor, ', ', listOfComp[i].customcolor[j].txcolor);
                    Inc(j);
                end;
                writeln;
            end;
        end;
        
        if modifier = ModDebug then
            write('COXgen Debug: Creating binary file... ');
        Assign(f_bin, FILE_BIN_NAME);
        Rewrite(f_bin);
        for i := Low(listOfComp) to High(listOfComp) do begin
            write(f_bin, listOfComp[i]);
        end;
        Close(f_bin);
        if modifier = ModDebug then
            writeln('Done!');
        
        if modifier <> ModQuiet then
            writeln('COXgen Warning: Please, check if the information obtained (displayed above) is the correct one. If it''s not, review the ''' + FILE_TEXT_NAME + ''' file.');
    except
        on ex : exception do
            ShowException(ex);
    end
    finally
        if FileExists(ExtractFilePath(ParamStr(0)) + FILE_TEXT_NAME) then
            Close(f_text);
    end;
    writeln;
end.
