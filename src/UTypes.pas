(* * * * * * * * * * * * * * * === UTypes ===  * * * * * * * * * * * * *
 *      Version: 1.0.0 beta-1                                          *
 * Release date: June 12th, 2014                                       *
 *      License: GNU GPL 3.0  (included with the source)               *
 *       Author: Igor Nunes, aka thoga31 @ www.portugal-a-programar.pt *
 *                                                                     *
 *  Description: COX Application main types definitions                *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)

unit UTypes;

interface

const MAXDEF = 10;
      FILE_TEXT_NAME = 'compilers.txt';
      FILE_BIN_NAME  = 'compilers.def';

type TColorDefinition = record
        msg : string;
        bkcolor : word;
        txcolor : word;
     end;
     TCompiler = record
        access       : string;
        language     : string;
        binary       : record
            windows : string;
            unix    : string;
        end;
        defaultcolor : TColorDefinition;
        customcolor  : array [1..MAXDEF] of TColorDefinition;
     end;
     TAppModifier = (ModDefault, ModQuiet, ModDebug);

implementation
    
end.
