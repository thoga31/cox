(* * * * * * * * * * * * * === UUIIntegrator === * * * * * * * * * * * *
 *      Version: 1.0.0 beta-1                                          *
 * Release date: June 12th, 2014                                       *
 *      License: GNU GPL 3.0  (included with the source)               *
 *       Author: Igor Nunes, aka thoga31 @ www.portugal-a-programar.pt *
 *                                                                     *
 *  Description: COX UI integration (work in progress)                 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)

{$mode objfpc}
unit UUIIntegrator;

interface
uses crt, sysutils;

procedure ShowException(ex : Exception);
procedure ShowHeader;


implementation
uses UAppInfo;

procedure ShowException(ex : Exception);
begin
    textcolor(12);
    writeln('(Exception ', ex.classname, ') => ', ex.message);
    textcolor(7);
end;


procedure ShowHeader;
begin
    textcolor(15);
    writeln(APPNAME + ', ' + APPCOMPLETENAME + ' (' + APPVERSION + ')');
    writeln('By ' + APPAUTHOR + ', aka ' + APPAUTHOR_NICK + ' @ ' + APPAUTHOR_NICK_SITE);
    textcolor(7);
end;

end.
