(* * * * * * * * * * * * * ==== UAppInfo ====  * * * * * * * * * * * * *
 *      Version: 1.0.0 beta-1                                          *
 * Release date: June 12th, 2014                                       *
 *      License: GNU GPL 3.0  (included with the source)               *
 *       Author: Igor Nunes, aka thoga31 @ www.portugal-a-programar.pt *
 *                                                                     *
 *  Description: COX Information and help                              *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)

{$mode objfpc}
unit UAppInfo;

interface

const APPNAME             = 'COX';
      APPCOMPLETENAME     = 'Compiler Output eXtender';
      APPVERSION          = '1.0.0 beta-1';
      APPDATE             = 'June 12th, 2014';
      APPBUILD            = '071214100001';
      APPAUTHOR           = 'Igor Nunes';
      APPAUTHOR_NICK      = 'thoga31';
      APPAUTHOR_NICK_SITE = 'www.portugal-a-programar.pt';


function SpecialModes : boolean;
procedure About;
procedure Help;




implementation
uses crt, UTypes;

function SpecialModes : boolean;
begin
    SpecialModes := true;
    if ParamCount > 0 then begin
        case ParamStr(1) of
            '-?', '--help' : Help;
            '--about'      : About;
        else
            SpecialModes := false;
        end;
    end else
        SpecialModes := false;
end;


procedure About;
begin
    writeln('   Date: ', APPDATE);
    writeln('  Build: ', APPBUILD);
    writeln;
    writeln('  Colorize your compiler output ;)');
end;


procedure Help;
begin
    textcolor(14);
    writeln('Usage: cox [-d | -q] compiler file');
    textcolor(7);
    writeln('''compiler'' must be defined in the ''', FILE_TEXT_NAME, ''' file!');
    textcolor(15);
    writeln('Modifier   Description');
    textcolor(7);
    writeln('   -q      Mode quiet: compiler output isn''t shown. Not recommended.');
    writeln('   -d      Mode debug: gives the maximum information possible.');
    textcolor(14);
    writeln; writeln('About COXgen, the COX Binary File Generator:');
    textcolor(7);
    writeln('It uses the same modifiers. No more arguments are needed.');
    writeln('The file ''', FILE_TEXT_NAME, ''' must exist! If it doesn''t, the current binary is NOT replaced.');
    textcolor(7);
end;



end.
