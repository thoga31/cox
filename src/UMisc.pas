(* * * * * * * * * * * * * * * === UMisc === * * * * * * * * * * * * * *
 *      Version: 1.0.0 beta-1                                          *
 * Release date: June 12th, 2014                                       *
 *      License: GNU GPL 3.0  (included with the source)               *
 *       Author: Igor Nunes, aka thoga31 @ www.portugal-a-programar.pt *
 *                                                                     *
 *  Description: Miscellaneous functions and procedures                *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)

{$mode objfpc}
unit UMisc;

interface
uses classes, sysutils;

type TStringArray = array of string;

function Split(s : string; const DELIMITER : char = ' ') : TStringArray;
function ExtractFromQuote(var s : string) : string;



implementation

function Split(s : string; const DELIMITER : char = ' ') : TStringArray;
var i    : word;
    temp : string = '';
begin
    SetLength(Split, 0);
    for i := 1 to Length(s) do begin
        if (s[i] = DELIMITER) or (i = Length(s)) then begin
            SetLength(Split, Length(Split)+1);
            if i = Length(s) then
                temp := Concat(temp, s[i]);
            Split[High(Split)] := temp;
            temp := '';
        end else
            temp := Concat(temp, s[i]);
    end;
end;


function ExtractFromQuote(var s : string) : string;
var i : byte;
begin
    ExtractFromQuote := '';
    for i := Pos('"', s) + 1 to Length(s) do begin
        if s[i] = '"' then
            break
        else
            ExtractFromQuote := Concat(ExtractFromQuote, s[i]);
    end;
    Delete(s, Pos('"', s), i - Pos('"', s) + 1);
end;

end.
