COX, Compiler Output eXtender

1.0.0 beta-1

Igor Nunes, aka thoga31 @ www.portugal-a-programar.pt

Licensed under the GNU GPL 3.0 (https://www.gnu.org/copyleft/gpl.html)

Language: Object Pascal (using Free Pascal Compiler, only)

This application colorizes the output of your compiler. It must be set in the file "compilers.txt" and the binary file must be generated using 'coxgen'.

This project is on its beginning, so it's a little bit incomplete. Give your contribution! ;)
